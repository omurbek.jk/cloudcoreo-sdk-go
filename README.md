# CloudCoreo-sdk-go

[![GoDoc](https://godoc.org/github.com/CloudCoreo/cloudcoreo-sdk-go/client?status.svg)](https://godoc.org/github.com/CloudCoreo/cloudcoreo-sdk-go/client)
[![Go Report Card](https://goreportcard.com/badge/github.com/CloudCoreo/cloudcoreo-sdk-go)](https://goreportcard.com/report/github.com/CloudCoreo/cloudcoreo-sdk-go)
[![Build Status](https://travis-ci.org/CloudCoreo/cloudcoreo-sdk-go.svg?branch=master)](https://travis-ci.org/CloudCoreo/cloudcoreo-sdk-go)
[![Coverage Status](https://coveralls.io/repos/github/CloudCoreo/cloudcoreo-sdk-go/badge.svg?branch=master)](https://coveralls.io/github/CloudCoreo/cloudcoreo-sdk-go?branch=master)

CoreoCoreo SDK written in Go

## Install
**DISCLAIMER: This software is still under development -- use at your own peril for now**

```
go get github.com/CloudCoreo/cloudcoreo-sdk-go
```

## Usage

```
package main

import (
	"context"
	"fmt"

	"github.com/CloudCoreo/cloudcoreo-sdk-go/client"
)

func main() {
	ctx := context.Background()
	c, _ := client.NewClient("Your key ID", "Your key secret")

	teams, _ := c.GetTeams(ctx)

	fmt.Println(teams[0])

}

```

## Community, discussion, contribution, and support

GitHub's Issue tracker is to be used for managing bug reports, feature requests, and general items that need to be addressed or discussed.

From a non-developer standpoint, Bug Reports or Feature Requests should be opened being as descriptive as possible.

### Code of conduct

Participation in the CloudCoreo community is governed by the [Coreo Code of Conduct](code-of-conduct.md).
